const { Plugin } = require('koa-oai-router');
const path = require('path');
const assert = require('assert');
const lodash = require('lodash');

const debug = require('debug')('koa-oai-router:AutoLoaderPlugin');

class AutoLoaderPlugin extends Plugin {
  constructor(args) {
    super(args);

    this.pluginName = 'auto-load';
    this.field = 'summary';
  }

  async handler(docOpts, args) {
    assert(lodash.isPlainObject(args) && args.path, 'no path provided!');
    // trick used to make api working with k8s config, when API accesspoint is
    // after standard front url
    // e.g. : front = test.domain.net, api = test.domain.net/api
    let filepath = docOpts.endpoint;
    if (args.trimStart && filepath.indexOf(args.trimStart) === 0) {
        filepath = filepath.replace(args.trimStart, '');
    }

    const modulePath = path.resolve(path.normalize(`${args.path}/${filepath.replace(/\:/g, '_')}`));

    debug(`module path: ${modulePath}`);

    const module = require(modulePath);
    const handler = docOpts.operation;

    assert(module, `middleware file [${modulePath}] not exists!`);
    assert(lodash.isFunction(module[handler]), `module [${modulePath}] has no function [${handler}]!`);

    return async (ctx, next) => {
      await next();
      await module[handler](ctx);
    };
  }
}

module.exports = AutoLoaderPlugin;
