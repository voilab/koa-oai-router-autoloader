# koa-oai-router-autoloader

[router]: https://github.com/BiteBit/koa-oai-router

Autoloader middleware plugin for [koa-oai-router][router]

# Installation

npm
```bash
npm i --save koa-oai-router-autoloader
```

yarn
```bash
yarn add koa-oai-router-autoloader
```

# Options
|field|type|info|
|---|---|---|
|**path**|`string`| Relative or absolute path to the route modules |


# Usage
In this example, we will load middlewares from `./controllers` directory.

```js
const Koa = require('koa');
const Router = require('koa-oai-router');
const autoLoaderPlugin = require('koa-oai-router-autoloader');

const app = new Koa();
const router = new Router({
  apiDoc: './api'
});

router.mount(autoLoaderPlugin, {
  path: './controllers'
});

app.use(router.routes());
```

```js
// ./controllers/pets.js

module.exports = {
  get: (ctx) => {
    ctx.body = {};
  }
};
```

```yaml
# ./api/paths/pets.yaml

/pets:
  get:
    description: "Returns all pets from the system that the user has access to"
    parameters:
      - name: "tags"
        in: "query"
        description: "tags to filter by"
        required: false
        type: "array"
        items:
          type: "string"
        collectionFormat: "csv"
      - name: "limit"
        in: "query"
        description: "maximum number of results to return"
        required: false
        type: "integer"
        format: "int32"
    responses:
      "200":
        description: "pet response"
        schema:
          type: "array"
          items:
            $ref: "#/definitions/Pet"
```
